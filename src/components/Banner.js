import React from 'react';
import { InputGroup,
	FormControl, Button } from 'react-bootstrap';

const Banner = (props) => {
    return (
        <div className="Banner container">
           <div className='row row-banner'>
                <div className='col-banner col-lg-10 offset-lg-1 col-md-10 offset-md-1 col-sm-10 offset-sm-1 col-xs-10 offset-xs-1'>
                    <h2>Приклучи се на 1350 ентузијасти и учи дигитални вештини. Бесплатно</h2>
                </div>
                <div className="col-banner col-lg-6 offset-lg-3 col-md-6 offset-md-3 col-sm-8 offset-sm-2 col-xs-8 offset-xs-2">
                <InputGroup className="mb-3">
						<FormControl className="input-email"
							placeholder="Email Address"
							aria-label="Recipient's username"
							aria-describedby="basic-addon2"
						/>
						<InputGroup.Append>
							<Button variant="outline-secondary">Пријави се</Button>
						</InputGroup.Append>
					</InputGroup>
                </div>
                <div className="col-banner col-lg-6 offset-lg-3 col-md-6 offset-md-3 col-sm-6 offset-sm-3 col-xs-6 offset-xs-3">
                    <p>Можеш да се исклучиш од листата на меилови во секое време!</p>
                </div>
           </div>
        </div>
    );
}

export default Banner;


