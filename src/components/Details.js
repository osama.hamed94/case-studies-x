import React, { Component } from 'react';
import marketingLessons from '../assets/lessons/marketingLessons.json';
import businessLessons from '../assets/lessons/businessLessons.json';
import uxuiLessons from '../assets/lessons/uxuiLessons.json';
import datascienceLessons from '../assets/lessons/datascienceLessons.json';
import programmingLessons from '../assets/lessons/programmingLessons.json';
import designLessons from '../assets/lessons/designLessons.json';
import cardsData from './CardsData.json';

class Details extends Component {
	constructor(props) {
		super(props);

		this.state = {
			lessons: [],
			cardsBanner: []
		};
	};

	componentDidMount() {
		if (this.props.match.params.subject == 'marketing') {
			this.setState({
				lessons: marketingLessons,
				cardsBanner: cardsData.slice(0, 1)
			});
		};
		if (this.props.match.params.subject == 'business') {
			this.setState({
				lessons: businessLessons,
				cardsBanner: cardsData.slice(1, 2)
			});
		};
		if (this.props.match.params.subject == 'uxui') {
			this.setState({
				lessons: uxuiLessons,
				cardsBanner: cardsData.slice(2, 3)
			});
		};
		if (this.props.match.params.subject == 'datascience') {
			this.setState({
				lessons: datascienceLessons,
				cardsBanner: cardsData.slice(3, 4)
			});
		};
		if (this.props.match.params.subject == 'programming') {
			this.setState({
				lessons: programmingLessons,
				cardsBanner: cardsData.slice(4, 5)
			});
		};
		if (this.props.match.params.subject == 'design') {
			this.setState({
				lessons: designLessons,
				cardsBanner: cardsData.slice(5, 6)
			});
		};
	};

	render() {
		return (
			<div className="Details container-fluid">
				<div className="row row-lessons">
					<div className="col-lg-8 order-lg-1 col-md-8 order-md-1 col-sm-12 order-sm-2 col-xs-12 order-2">
						<div className="row">
							<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								{this.state.lessons.map((el, i) => (
									<a className="card-a" href={el.link} target="_blank">
										<div key={i} className="card card-detail card-gray">
											<div className="card-body body-gray">
												<span className="number">{`# ${el.id}`}</span>
												<h5 className="h5-title">{el.title}</h5>
												<span className="material-icons">{el.icon}</span>
												<small className="date">{el.date}</small>
												<p className="detail-body">{el.body}</p>
											</div>
										</div>
									</a>
								))}
							</div>
						</div>
					</div>
					<div className="Card cols col-lg-4 order-lg-2 col-md-4 order-md-2 col-sm-12 order-sm-1 col-xs-12 order-1">
						<div className="row">
							<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								{this.state.cardsBanner.map((el, i) => (
									<div key={i} className="card card-gray">
										<div className="card-body body-gray">
											<small className="card-text small-gray">{el.lessons} лекции</small>
											<h4 className="card-title">{el.title}</h4>
											<p className="card-text">{el.body}</p>
											<button className="btn show-more">
												Повеќе <span class="material-icons">arrow_forward</span>
											</button>
										</div>
									</div>
								))}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	};
};

export default Details;
