import React from 'react';
import styled, { keyframes } from 'styled-components';
import { zoomInDown } from 'react-animations';

const NotFound = (props) => {
    const ZoomInDown = styled.h2`animation: 5s ${keyframes`${zoomInDown}`} infinite;`;

	return (
		<div className="not-found">
			<ZoomInDown className="h2-notfound text-center">
				CONTENT NOT FOUND, PLEASE GO BACK TO HOMEPAGE! <br />
				<span className="material-icons smile">sentiment_satisfied_alt</span>
			</ZoomInDown>
		</div>
	);
};

export default NotFound;
