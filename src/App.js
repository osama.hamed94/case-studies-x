import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Header from './components/Header';
import Banner from './components/Banner';
import HomePage from './components/HomePage';
import Details from './components/Details';
import NotFound from './components/NotFound';

function App() {
	return (
		<Router>
			<div className="App">
				<Header />
				<Banner />
				<Switch>
					<Route exact path="/" render={() => <HomePage />} />
					<Route path="/details/:subject" render={(props) => <Details {...props} />} />
					<Route component={NotFound} />
				</Switch>
			</div>
		</Router>
	);
}

export default App;
