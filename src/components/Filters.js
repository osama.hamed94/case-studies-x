import React, { Component } from 'react';

class Filters extends Component {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="Filters row">
				<div className="justify-content-center col-filters col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h5>Филтрирај по категорија</h5>
				</div>
				<div className="off-on">
					<label className="switcher">
						<input onChange={this.props.onOff} type="checkbox" checked={this.props.checked} />
						<span className="slider" />
					</label>
				</div>
				<div className="col-filters col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div className="filter">
						<button onClick={(e) => this.props.cardsFilter(e)} className={this.props.toggleAll ? "btn all active" : "btn all"}>Сите</button>
					</div>
					<div className="filter">
						<button onClick={(e) => this.props.cardsFilter(e)} className={this.props.toggleMarketing ? "btn marketing active" : "btn marketing"}>Маркетинг</button>
					</div>
					<div className="filter">
						<button onClick={(e) => this.props.cardsFilter(e)} className={this.props.toggleBusiness ? "btn business active" : "btn business"}>Бизнис</button>
					</div>
					<div className="filter">
						<button onClick={(e) => this.props.cardsFilter(e)} className={this.props.toggleUXUI ? "btn uxui active" : "btn uxui"}>UX/UI</button>
					</div>
					<div className="filter">
						<button onClick={(e) => this.props.cardsFilter(e)} className={this.props.toggleDataScience ? "btn data-science active" : "btn data-science"}>Data Science</button>
					</div>
					<div className="filter">
						<button onClick={(e) => this.props.cardsFilter(e)} className={this.props.toggleProgramming ? "btn programming active" : "btn programming"}>Програмирање</button>
					</div>
					<div className="filter">
						<button onClick={(e) => this.props.cardsFilter(e)} className={this.props.toggleDesign ? "btn design active" : "btn design"}>Дизајн</button>
					</div>
				</div>
			</div>
		);
	}
}

export default Filters;
