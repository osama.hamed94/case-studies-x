import React, { Component } from 'react';
import Filters from './Filters';
import cardsData from './CardsData.json';
import List from './List';

class HomePage extends Component {
	constructor(props) {
		super(props);

		this.allCards = cardsData;

		this.state = {
			cards: [],
			cardsFiltered: [],
			toggleActiveAll: false,
			toggleActiveMarketing: false,
			toggleActiveBusiness: false,
			toggleActiveUXUI: false,
			toggleActiveDataScience: false,
			toggleActiveProgramming: false,
			toggleActiveDesign: false,
			checked: true
		};
	}

	// Show all cards on HomePage load 
	componentDidMount() {
		this.setState({
			toggleActiveAll: !this.state.toggleActiveAll,
			cards: this.allCards
		});
	};

	// Function for filtering cards by category
	cardsFilter = (e) => {
		let cardsNew = this.state.cardsFiltered.slice();

		if (e.target.innerText === 'Сите') {
			this.setState({
				toggleActiveAll: !this.state.toggleActiveAll,
				toggleActiveMarketing: false,
				toggleActiveBusiness: false,
				toggleActiveUXUI: false,
				toggleActiveDataScience: false,
				toggleActiveProgramming: false,
				toggleActiveDesign: false,
				cards: this.allCards,
				cardsFiltered: []
			});
			if (this.state.toggleActiveAll === true) {
				this.setState({
					cards: []
				});
			};
		};

		if (e.target.innerText === 'Маркетинг') {
			let marketingCard = this.allCards.find((el) => el.title === 'Маркетинг');

			cardsNew.push(marketingCard);

			this.setState({
				toggleActiveAll: false,
				toggleActiveMarketing: !this.state.toggleActiveMarketing,
				cardsFiltered: cardsNew,
				cards: cardsNew
			});

			if (this.state.toggleActiveMarketing === true) {
				this.setState({
					cards: this.state.cards.filter((el) => el.title !== e.target.innerText),
					cardsFiltered: this.state.cards.filter((el) => el.title !== e.target.innerText)
				});
			};
		};

		if (e.target.innerText === 'Бизнис') {
			let businessCard = this.allCards.find((el) => el.title === 'Бизнис');

			cardsNew.push(businessCard);

			this.setState({
				toggleActiveAll: false,
				toggleActiveBusiness: !this.state.toggleActiveBusiness,
				cardsFiltered: cardsNew,
				cards: cardsNew
			});

			if (this.state.toggleActiveBusiness === true) {
				this.setState({
					cards: this.state.cards.filter((el) => el.title !== e.target.innerText),
					cardsFiltered: this.state.cards.filter((el) => el.title !== e.target.innerText)
				});
			};
		};

		if (e.target.innerText === 'UX/UI') {
			let uxuiCard = this.allCards.find((el) => el.title === 'UX/UI');

			cardsNew.push(uxuiCard);

			this.setState({
				toggleActiveAll: false,
				toggleActiveUXUI: !this.state.toggleActiveUXUI,
				cardsFiltered: cardsNew,
				cards: cardsNew
			});

			if (this.state.toggleActiveUXUI === true) {
				this.setState({
					cards: this.state.cards.filter((el) => el.title !== e.target.innerText),
					cardsFiltered: this.state.cards.filter((el) => el.title !== e.target.innerText)
				});
			};
		};

		if (e.target.innerText === 'Data Science') {
			let datascienceCard = this.allCards.find((el) => el.title === 'Data Science');

			cardsNew.push(datascienceCard);

			this.setState({
				toggleActiveAll: false,
				toggleActiveDataScience: !this.state.toggleActiveDataScience,
				cardsFiltered: cardsNew,
				cards: cardsNew
			});

			if (this.state.toggleActiveDataScience === true) {
				this.setState({
					cards: this.state.cards.filter((el) => el.title !== e.target.innerText),
					cardsFiltered: this.state.cards.filter((el) => el.title !== e.target.innerText)
				});
			};
		};

		if (e.target.innerText === 'Програмирање') {
			let programmingCard = this.allCards.find((el) => el.title === 'Програмирање');

			cardsNew.push(programmingCard);

			this.setState({
				toggleActiveAll: false,
				toggleActiveProgramming: !this.state.toggleActiveProgramming,
				cardsFiltered: cardsNew,
				cards: cardsNew
			});

			if (this.state.toggleActiveProgramming === true) {
				this.setState({
					cards: this.state.cards.filter((el) => el.title !== e.target.innerText),
					cardsFiltered: this.state.cards.filter((el) => el.title !== e.target.innerText)
				});
			};
		};

		if (e.target.innerText === 'Дизајн') {
			let designCard = this.allCards.find((el) => el.title === 'Дизајн');

			cardsNew.push(designCard);

			this.setState({
				toggleActiveAll: false,
				toggleActiveDesign: !this.state.toggleActiveDesign,
				cardsFiltered: cardsNew,
				cards: cardsNew
			});

			if (this.state.toggleActiveDesign === true) {
				this.setState({
					cards: this.state.cards.filter((el) => el.title !== e.target.innerText),
					cardsFiltered: this.state.cards.filter((el) => el.title !== e.target.innerText)
				});
			};
		};

	};

	// Function for cards-change on switch (change) 'on-off' ('checked': 'on' - mode)
	switcherOnOff = () => {
		this.setState({
			checked: !this.state.checked
		});
	};

	render() {
		return (
			<div className="HomePage container-fluid">
				<Filters
					toggleAll={this.state.toggleActiveAll}
					toggleMarketing={this.state.toggleActiveMarketing}
					toggleBusiness={this.state.toggleActiveBusiness}
					toggleUXUI={this.state.toggleActiveUXUI}
					toggleDataScience={this.state.toggleActiveDataScience}
					toggleProgramming={this.state.toggleActiveProgramming}
					toggleDesign={this.state.toggleActiveDesign}
					cardsFilter={this.cardsFilter}
					onOff={this.switcherOnOff}
					checked={this.state.checked}
				/>
				<List cardsFilter={this.cardsFilter} checked={this.state.checked} cards={this.state.cards} />
			</div>
		);
	}
}

export default HomePage;
