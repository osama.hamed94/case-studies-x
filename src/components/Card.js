import React from 'react';
import { Link } from 'react-router-dom';
import styled, { keyframes } from 'styled-components';
import { flipInY, zoomIn } from 'react-animations';

const Card = (props) => {
	// Animations
    const FlipInY = styled.img`animation: 5s ${keyframes`${flipInY}`} infinite;`;
    const ZoomIn = styled.div`animation: 7s ${keyframes`${zoomIn}`} infinite;`;

	return (
		<div className="Card cols col-lg-4 col-md-6 col-sm-12 col-xs-12">
			<Link className="card-a" to={'/details/' + props.cardTitle2.toLowerCase()}>
				{!props.checked ? (
					<ZoomIn className="card h-100 card-gray">
						<div className="card-body body-gray">
                        <small className="card-text small-gray">{props.cardLessons} лекции</small>
                        <h4 className="card-title">{props.cardTitle}</h4>
                            <p className="card-text">{props.cardBody}</p>
						</div>
					</ZoomIn>
				) : (
					<div className="card h-100">
						<FlipInY src={props.cardImg} className="card-img-top" alt={props.cardTitle} />
						<div className="card-body">
							<h4 className="card-title">{props.cardTitle}</h4>
							<p className="card-text">{props.cardBody}</p>
						</div>
						<div className="card-footer">
							<small className="card-text text-muted">{props.cardLessons} лекции</small>
						</div>
					</div>
				)}
			</Link>
		</div>
	);
};

export default Card;
